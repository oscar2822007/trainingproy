import { Component, ViewChild, OnInit, Inject, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiServiceService } from 'src/app/services/api-service.service';
import {MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalAddComponent } from '../modal-add/modal-add.component';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  displayedColumns: string[] = ['id', 'name', 'createdById', 'branchId', 'createdAt', 'status', 'editar', 'delete'];
  dataSource!: any;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort!: MatSort;
  constructor(private routerlink: Router, private apiService:ApiServiceService , private matDialog: MatDialog, private dialog: MatDialog){

  }
 
 
  ngOnInit(): void {
    if (!localStorage.getItem('token')) {
      this.routerlink.navigate(['/']);
    }
    this.load();
  }

  public initDataSource(data:any){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
  } 
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteOrigin(value: any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.apiService.deleteOrigin(value.id).subscribe((response) =>{
         
          if(response){
            swalWithBootstrapButtons.fire(
              'Deleted!',
              'The Origin has been deleted.',
              'success'
            )
          } else {
            swalWithBootstrapButtons.fire(
              'Deleted!',
              "Couldn't delete this Origin.",
              'error'
            )
          }
          
        })
       
      } else if (
        
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Origin is safe :)',
          'error'
        )
      }
    })
  }
  
  load(){
    this.apiService.getAllOrigins().subscribe((data) => {
     
      this.getName(data); 
    });
  }

  getName(data:any){
    
    for (let i = 0; i < data.length; i++){
     
      this.apiService.getNamesById(data[i]['createdById']).subscribe((response) =>{
       
        data[i]['createdById']= response['name'];
      })
    }
    this.getBranch(data);
  }

  getBranch(data:any){
    
    for (let i = 0; i < data.length; i++){
      
      this.apiService.getBranchbyId(data[i]['branchId']).subscribe((response) =>{
        
        data[i]['branchId']= response['name'];
      })
    }
    this.initDataSource(data);
  }

  openDialog(value: any) {
    const dialogConfig = new MatDialogConfig();
      dialogConfig.data = value;
      const dialogRef = this.dialog.open(ModalAddComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        
        this.load();
      });
  }

  logout() {
    localStorage.clear();
    this.routerlink.navigate(['/']);

  }
  

}

