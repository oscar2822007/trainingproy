import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formulario: FormGroup;

  constructor(private routerlink: Router,
    private APISERVICE: ApiServiceService,
    public form: FormBuilder,
  )  { 

    this.formulario = this.form.group({
      email: [""],
      password: [""],
    });

  }

  ngOnInit(): void {
  }

  //Call function log from ApiService Service.
  LogIn() {



    this.APISERVICE.GetToken(this.formulario.value).subscribe(response => {
      if (response["id"] == null){
        
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Email or password does not match.',
          showConfirmButton: false,
          timer: 15000
        });

      }else{
        console.log(response);
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Welcome '+ this.formulario.value.email,
          showConfirmButton: false,
          timer: 15000
        })
        localStorage.setItem('token', response["id"]);
        localStorage.setItem('userId', response["userId"]);
        this.routerlink.navigate(['/dashboard']);
      }
    })
  }

}
