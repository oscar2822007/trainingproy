import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import { Observable } from 'rxjs';
import { ApiServiceService } from 'src/app/services/api-service.service';
import Swal from 'sweetalert2';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-modal-add',
  templateUrl: './modal-add.component.html',
  styleUrls: ['./modal-add.component.scss']
})
export class ModalAddComponent implements OnInit{
  public form!: FormGroup;
  id: any;
  dealers: any;
  filtered: any;
  myControl = new FormControl('');
  filteredOptions!: Observable<string[]>;
  searchName: boolean = false;
  searchCode: boolean = true;
  constructor( public dialogRef: MatDialogRef<ModalAddComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public fb: FormBuilder, private dialog: MatDialog, private ApiSve: ApiServiceService){

  }
  
  ngOnInit(): void {
    console.log(this.data);
    this.load();
    this.form = this.fb.group({
      branchId: [null, Validators.required],
      name: [null, Validators.required],
      createdById: localStorage.getItem('userId')
    });

 

  }


  load(){
    this.ApiSve.getAllBranches().subscribe((dta) => {
      
      this.dealers = dta;
      
      //Barrido para cargar datos en caso de ser llamado desde editar.
      for (let i = 0; i < this.dealers.length; i++) {
        
        if(this.data.branchId == this.dealers[i]['name']){
          this.id = this.data.id
          this.form.patchValue({
            branchId: this.dealers[i]['id'],
            name: this.data.name,
            
          });
          
        }

      }
      
      this.filtered = this.dealers.slice();
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  onSubmit(){
    
    if(this.data){//EDITAR
      this.ApiSve.UpthOrigin(this.id, this.form.value).subscribe((dataR) => {
        if(dataR){
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Origins model Updated Succesfuly!',
            showConfirmButton: false,
            timer: 1500
          });
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Something is wrong!',
            showConfirmButton: false,
            timer: 1500
          });
        }
      }, err => {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Something is wrong!',
          showConfirmButton: false,
          timer: 1500
        });
      });
     
    }else{//AGREGAR
      
      this.ApiSve.PostOrigin(this.form.value).subscribe((data) => {
        if(data){
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Origin model Created!',
            showConfirmButton: false,
            timer: 1500
          });
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Something is wrong!',
            showConfirmButton: false,
            timer: 1500
          });
        }
      }, err => {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Something is wrong!',
          showConfirmButton: false,
          timer: 1500
        });
      });
      
  
    }
    this.onNoClick();

}
}
