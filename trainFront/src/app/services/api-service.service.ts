import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import { Token } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(private clienteHttp: HttpClient, private dialog: MatDialog) { 

  }
 
  //Function 'GetToken' calls Login from the Api in loopback.
  GetToken(form: any): Observable<any> {
    let params = JSON.stringify(form);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.clienteHttp.post("https://apitest.adroitoverseas.net/api/AppUsers/login", params, { headers: headers });
  }

  //Function 'getAllOrigins' to get all origin.
  getAllOrigins() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
     
    }) 
    return this.clienteHttp.get("https://apitest.adroitoverseas.net/api/Origins?access_token="+ localStorage.getItem('token') , {headers:headers});
  }
  //Function 'getAllBranches' to get all branches.
  getAllBranches() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
     
    }) 
    return this.clienteHttp.get("https://apitest.adroitoverseas.net/api/Branches?access_token="+ localStorage.getItem('token') , {headers:headers});
  }

  //Gets getNamesById to search
  getNamesById(id: any): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
     
    })
    return this.clienteHttp.get('https://apitest.adroitoverseas.net/api/AppUsers/' + id + '?access_token=' + localStorage.getItem('token'), {headers:headers});
  }
  
  //Function to get the branches for ID
  getBranchbyId(id: any): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
     
    })
    return this.clienteHttp.get('https://apitest.adroitoverseas.net/api/Branches/' + id + '?access_token=' + localStorage.getItem('token'), {headers:headers});
  }

  //Function to post an Origin model.
  PostOrigin(form: any): Observable<any> {
    let params = JSON.stringify(form);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.clienteHttp.post("https://apitest.adroitoverseas.net/api/Origins?access_token=" + localStorage.getItem('token'), params, { headers: headers });
  }

  //Patch for origins
  UpthOrigin(id: any, form: any): Observable<any> {
    console.log(form, id)
    let params = JSON.stringify(form);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.clienteHttp.patch("https://apitest.adroitoverseas.net/api/Origins/" + id + "?access_token=" + localStorage.getItem('token'), params, { headers: headers });
  }
  

  //Delete function
  deleteOrigin(id: any): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
     
    })
    return this.clienteHttp.delete('https://apitest.adroitoverseas.net/api/Origins/' + id + '?access_token=' + localStorage.getItem('token'), {headers:headers});
  }


}
